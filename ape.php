<?php

require_once("animal.php");

class Ape extends Animal{
    public $legs = 2;
    public $Suara = "Auooo";

    public function yell()
    {
        echo $this->Suara;
    }
}